'use strict';

const express = require('express');
const router = express.Router();

const Test2Helper = helper('Test2');

router.post('/', function (req, res) {
    Test2Helper.combine(req.body.list1, req.body.list2, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result})
    });
});

module.exports = router;
