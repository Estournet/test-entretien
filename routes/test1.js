'use strict';

const express = require('express');
const router = express.Router();

const Test1Helper = helper('Test1');

router.post('/for', function (req, res) {
    Test1Helper.forMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result});
    })
});
router.post('/foreach', function (req, res) {
    Test1Helper.foreachMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result});
    })
});
router.post('/while', function (req, res) {
    Test1Helper.whileMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result});
    })
});
router.post('/recursion', function (req, res) {
    Test1Helper.recursionMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result});
    })
});
router.post('/', function (req, res) {
    const data = {};
    Test1Helper.forMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        data.for = result;
    });
    Test1Helper.recursionMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        data.recursion = result;
    });
    Test1Helper.whileMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        data.while = result;
    });
    Test1Helper.foreachMethod(req.body.list, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        data.foreach = result;
    });
    res.json({lol: data})
});

module.exports = router;
