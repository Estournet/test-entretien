'use strict';

const express = require('express');
const router = express.Router();

const Test3Helper = helper('Test3');

router.post('/', function (req, res) {
    Test3Helper.highestPath(req.body.matrix, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json({result})
    });
});

module.exports = router;
