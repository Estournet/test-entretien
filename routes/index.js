'use strict';

const express = require('express');
const router = express.Router();
const DefaultHelper = helper('Default');

router.get('/', function (req, res) {
    res.send("ok");
});
router.post('/search', function (req, res) {
    DefaultHelper.default(req.body, function (err, result) {
        if (err) return res.status(500).json({'error': err});
        res.json(result);
    })
});

router.use("/default", route("default"));
router.use("/test1", route("test1"));
router.use("/test2", route("test2"));
router.use("/test3", route("test3"));

module.exports = router;