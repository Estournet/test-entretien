'use strict';

class Test3Model {
    highestPath(matrix, callback) {
        let pathSum = 0, i = 0, j = 0, count = 0;

        while (count < matrix.length + matrix[0].length - 1) {
            pathSum += matrix[i][j];
            count++;

            // If we reach the bottom border
            if (i === matrix.length - 1)
                j++;
            else {
                // If we reach the right border
                if (j === matrix[0].length - 1)
                    i++;
                else {
                    if (matrix[i + 1][j] > matrix[i][j + 1])
                        i++;
                    else
                        j++;
                }
            }
        }
        callback(null, pathSum);
    }
}

module.exports = new Test3Model();