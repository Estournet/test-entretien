'use strict';

class Test2Model {
    combine(list1, list2, callback) {
        const array = [];
        const biggestList = list1.length > list2.length ? list1.length : list2.length;

        for (let i = 0; i < biggestList; i++) {
            if (list1[i]) array.push(list1[i]);
            if (list2[i]) array.push(list2[i]);
        }

        callback(null, array);
    }
}

module.exports = new Test2Model();