'use strict';

class Test1Model {
    forMethod(array, callback) {
        let sum = 0;

        for (let i = 0; i < array.length; i++)
            sum += array[i];

        callback(null, sum);
    }

    foreachMethod(array, callback) {
        let sum = 0;

        array.forEach(nb => sum += nb);

        callback(null, sum);
    }

    whileMethod(array, callback) {
        let sum = 0, i = 0;

        while (i < array.length) {
            sum += array[i];
            i++;
        }

        callback(null, sum);
    }

    recursionMethod(array, callback) {
        const sum = (array) => {
            if (array.length === 0) return 0;
            else return array[0] + sum(array.slice(1));
        };

        callback(null, sum(array));
    }
}

module.exports = new Test1Model();