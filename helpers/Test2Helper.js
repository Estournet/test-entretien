'use strict';

const Test2 = model('Test2Model');

class Test2Helper {
    combine(list1, list2, callback) {
        Test2.combine(list1, list2, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }
}

module.exports = new Test2Helper();