'use strict';

const Test3 = model('Test3Model');

class Test3Helper {
    highestPath(matrix, callback) {
        Test3.highestPath(matrix, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }
}

module.exports = new Test3Helper();