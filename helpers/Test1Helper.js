'use strict';

const Test1 = model('Test1Model');

class Test1Helper {
    forMethod(params, callback) {
        Test1.forMethod(params, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }

    foreachMethod(params, callback) {
        Test1.foreachMethod(params, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }

    whileMethod(params, callback) {
        Test1.whileMethod(params, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }

    recursionMethod(params, callback) {
        Test1.recursionMethod(params, function (err, result) {
            if (err) return callback(err);
            callback(err, result);
        });
    }
}

module.exports = new Test1Helper();